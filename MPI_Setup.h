// ============================================================================
// 
//       Filename:  MPI_Setup.h
// 
//    Description:  All the header methods required to initialize MPI and 
//                  closing it.
// 
//        Created:  04/19/2010 09:38:20 AM
//       Compiler:  gcc
// 
//         Author:  Jose V. Trigueros (), j.v.trigueros@gmail.com
// 
// ============================================================================

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

// Global Variables
int gNumProcessors;
int gRank;
int gRoot;

// ===  FUNCTION  =============================================================
//         Name:  initializeMPI( int * argc, char ** argv )
//  Description:  Sets up MPI
// ============================================================================
void initializeMPI( int * argc, char ** argv );
