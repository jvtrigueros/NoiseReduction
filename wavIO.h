// ============================================================================
// 
//       Filename:  wavIO.h
// 
//    Description:  All the declarations that deal with input and out of the 
//                  WAV file will be created here.
// 
//        Created:  04/22/2010 12:03:07 AM
//       Compiler:  g**
// 
//         Author:  Jose V. Trigueros (), j.v.trigueros@gmail.com
// 
// ============================================================================

#ifndef WAVIO_H
#define WAVIO_H

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Global Variables
extern int HEADER_SIZE;

// ===  FUNCTION  =============================================================
//         Name:  readHeader(char *, char *)
//  Description:  Reads the header of the wave file and it stores it in an char
//                array.
// ============================================================================
void readHeader( char * filename, char * wavHeaderBuffer );
void writeHeader( char * filename, char * wavHeaderBuffer );

// ===  FUNCTION  =============================================================
//         Name:  readWAV(char *, int, char *, MPI_Comm)
//  Description:  Reads the WAV file using MPI IO
// ============================================================================
void readWAV(char * filename, int chunkSize, char * wavChunkBuffer, MPI_Comm comm);
void readWAVhankel(char * filename, int chunkSize, char * wavChunkBuffer, MPI_Comm comm);

// ===  FUNCTION  =============================================================
//         Name:  writeWAV(char *, int, char *, MPI_Comm)
//  Description:  Writes the WAV file using MPI IO
// ============================================================================
void writeWAV(char * filename, int chunkSize, char * wavChunkBuffer, char * wavHeaderBuffer, MPI_Comm comm);


// ===  FUNCTION  =============================================================
//         Name:  hankelize( char * vector , int length )
//  Description:  Hankelizes a vector and returns a row major order matrix
// ============================================================================
char * hankelize( char * vector, int length );

// ===  FUNCTION  =============================================================
//         Name:  printBuffer(char *, int )
//  Description:  Prints an array of chars.
// ============================================================================
void printBuffer( char * buffer , int length ) ;


#endif
