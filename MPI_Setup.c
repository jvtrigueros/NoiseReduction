// ============================================================================
// 
//       Filename:  MPI_Setup.c
// 
//    Description:  Implements the functions in the header file.
// 
//        Created:  04/19/2010 09:50:01 AM
//       Compiler:  gcc
// 
//         Author:  Jose V. Trigueros , j.v.trigueros@gmail.com
// 
// ============================================================================

#include "MPI_Setup.h"


void initializeMPI( int * argc, char ** argv )
{
    // Initialize MPI
    int mpiStatus = MPI_Init(argc,&argv);
    if( mpiStatus != MPI_SUCCESS )
    {
        // Something went wrong and we must quit
        printf("Something went wrong with MPI and we must quit");
        MPI_Abort(MPI_COMM_WORLD, mpiStatus );
    }

    // Returns the number of processors
    MPI_Comm_size(MPI_COMM_WORLD, &gNumProcessors);

    // Returns the current processor ID
    MPI_Comm_rank(MPI_COMM_WORLD, &gRank);

    // Sets the root ID
    gRoot = 0;
}
