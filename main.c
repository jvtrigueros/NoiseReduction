// ============================================================================
// 
//       Filename:  main.cpp
// 
//    Description:  Eliminates noise from a given WAV file.
// 
//        Created:  04/21/2010 01:12:28 PM
//       Compiler:  gcc
// 
//         Author:  Jose V. Trigueros , j.v.trigueros@gmail.com
// 
// ============================================================================

#include "wavIO.h"
#include "MPI_Setup.h"
#include "mkl_scalapack.h"
#include "PBblacs.h"
#include "PBpblas.h"
#include <string.h>
#include <math.h>

// Function Prototypes
extern void pdsyev_(char *, char *, int *, double *, int *, int *, int *, double *, double *, int *, int *, int *, double *, int *, int *);

extern void descinit_(int *, int *,int *,int *,int *,int *,int *,int *,int *,int *);

int main( int argc, char *argv[] )
{
    
    // Initialize MPI
    initializeMPI(&argc,argv);

    int blockDim = sqrt(gNumProcessors);
    int contxt;
    int nprow = blockDim, npcol = blockDim;
    int myrow, mycol;

    // name of the wav files for input and output
    char * in_waveFilename = "/scratch/jtriguer/test.wav";
//    char * out_waveFilename = "/scratch/jtriguer/small2.wav";
    char * wavHeaderBuffer;
    int filesize = 0;

    // Have rank 0 figure out the size of the file
    if ( gRank == gRoot )
    {
        // TODO: Could possibly move this to the readWAV function
        // Testing the header read
        wavHeaderBuffer = (char *)malloc(HEADER_SIZE);
        readHeader(in_waveFilename, wavHeaderBuffer);
    }

    // Get the file size using MPI
    MPI_File mpiWaveFile;
    MPI_Offset mpiFilesize;

    MPI_File_open(MPI_COMM_WORLD,in_waveFilename ,MPI_MODE_RDONLY,MPI_INFO_NULL, &mpiWaveFile);
    MPI_File_get_size(mpiWaveFile,&mpiFilesize);
    MPI_File_close(&mpiWaveFile);
    
    filesize = (int) mpiFilesize;

    // Determine the buffer offset, may not be needed after all
    int data = filesize - HEADER_SIZE;
    int bufferOffset = (int)ceil( ( (double)data )/gNumProcessors );
    
    // Determine the buffer size 
    int bufferSize = data/sqrt(gNumProcessors);
        //    int bufferSize = (filesize - HEADER_SIZE)/gNumProcessors; 

    // Create buffer for the data that will be read in
    char * wavBuffer =(char*) malloc( bufferSize );
    readWAVhankel( in_waveFilename, bufferSize, wavBuffer, MPI_COMM_WORLD);

    // Testing to see what was read
//    printf("Rank %d : ", gRank );
//    printBuffer( wavBuffer, bufferSize );

    // Hankelize current chunk
    char * currentHankelChunk = hankelize( wavBuffer, bufferSize ) ;

//    printBuffer( currentHankelChunk, blockDim*blockDim );
    
    // Trying out BLACS
    Cblacs_pinfo(&gRank, &gNumProcessors);
    Cblacs_get(0,0,&contxt);
    Cblacs_gridinit(&contxt, "R", nprow,npcol);
    Cblacs_gridinfo(contxt, &nprow, &npcol, &myrow, &mycol);

    // Intermediate Variables
    int N = blockDim;
    int BLK = blockDim*blockDim;
    int ZERO = 0;
    int ONE = 1;
    // Has to be 9 because that is the number of args in descinit_
    int desc_A[9];
    int info;
    
    // Taking a stab at ScaLAPACK
//    int mlocal = numroc_(&N, &BLK, &myrow, &ZERO, &nprow);
    int mlocal = blockDim;

    descinit_(desc_A, &N, &N, &BLK, &BLK, &ZERO, &ZERO, &contxt, &mlocal, &info);

    // Eigensolver variables
    char V = 'V';
    char U = 'U';
    double eigenValues[bufferOffset];
    double eigenVectors[bufferOffset*bufferOffset];
    int QRMEM = 2*bufferOffset-2;
//    int NRC = numroc_(&N, &BLK, &myrow, &ZERO, &nprow);
    int NRC = blockDim;
//    int LDC = Max(1,NRC);
//    int LWORK = 5*bufferOffset + bufferOffset*LDC + Max(1,QRMEM) + 1;
    double WORK[2];
    int LWORK = -1;

    // Using the PDSYEV function
    pdsyev_(&V, 
            &U, 
            &blockDim, 
            (double *) currentHankelChunk, 
            &ONE, 
            &ONE, 
            desc_A, 
            eigenValues, 
            eigenVectors, 
            &ONE, 
            &ONE, 
            desc_A, 
            WORK, 
            &LWORK, 
            &info ) ;

    // Writing File
//    writeWAV( out_waveFilename, bufferSize, wavBuffer, wavHeaderBuffer, MPI_COMM_WORLD);

    // Free stuff
    Cblacs_gridexit( 0 );
    free(wavBuffer);
    free(currentHankelChunk);
    if( gRank == gRoot ) 
        free(wavHeaderBuffer);
    // Finalize MPI
    MPI_Finalize();
    
    return EXIT_SUCCESS;
}// ----------  end of function main  ----------
