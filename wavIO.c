// ============================================================================
// 
//       Filename:  wavIO.c
// 
//    Description:  Implementation of the functions that deal with WAV file.
// 
//        Created:  04/22/2010 12:15:37 AM
//       Compiler:  gcc
// 
//         Author:  Jose V. Trigueros , j.v.trigueros@gmail.com
// 
// ============================================================================

#include "wavIO.h"
#include "MPI_Setup.h"

// Global Variables
int HEADER_SIZE = 44;

void readHeader( char * filename, char * wavHeaderBuffer )
{
    // Creating the file handle for this
    FILE * filePtr = fopen( filename, "rb" );
    
    // Allocating memory to store the header
//    wavHeaderBuffer = ( char * ) malloc( HEADER_SIZE );

    // Read the header
    fread( wavHeaderBuffer, sizeof(char), HEADER_SIZE, filePtr );

    // Close the file
    fclose(filePtr);
}

void writeHeader( char * filename, char * wavHeaderBuffer )
{
    // Create file handle
    FILE * filePtr = fopen( filename, "wb" );

    // Write the header
    fwrite(wavHeaderBuffer, sizeof(char), HEADER_SIZE, filePtr);

    // Close the file
    fclose(filePtr);
}

void readWAV(char * filename, int chunkSize, char * wavChunkBuffer, MPI_Comm comm)
{
    // Preparing Variables to do mpi file read
    MPI_File mpiWaveFile;
    MPI_Status status;
    
    // Create file pointer to the file being read
    MPI_File_open(comm,filename,MPI_MODE_RDONLY,MPI_INFO_NULL, &mpiWaveFile);
    
    // Determine reading offset
    int offset = (gRank * chunkSize) + HEADER_SIZE;

    // Set location to start reading from
    MPI_File_seek(mpiWaveFile, offset , MPI_SEEK_SET);

    // Reads the chunk of data and stores it
    MPI_File_read(mpiWaveFile, wavChunkBuffer, chunkSize, MPI_BYTE, &status);
    
    // Close mpi file
    MPI_File_close(&mpiWaveFile);
}

void readWAVhankel(char * filename, int chunkSize, char * wavChunkBuffer, MPI_Comm comm)
{
    // Matrix dimension
    int matrixDim = (int)sqrt(gNumProcessors);
    // Determine row and column of the big grid
    int row = gRank / matrixDim;
    int col = gRank % matrixDim;

    // Preparing Variables to do mpi file read
    MPI_File mpiWaveFile;
    MPI_Status status;
    
    // Create file pointer to the file being read
    MPI_File_open(comm,filename,MPI_MODE_RDONLY,MPI_INFO_NULL, &mpiWaveFile);
    
    // Determine reading offset
//    int offset = (gRank * chunkSize) + HEADER_SIZE;
    int offset = (row+col)*matrixDim + HEADER_SIZE;

    // Set location to start reading from
    MPI_File_seek(mpiWaveFile, offset , MPI_SEEK_SET);

    // Reads the chunk of data and stores it
    MPI_File_read(mpiWaveFile, wavChunkBuffer, chunkSize, MPI_BYTE, &status);
    
    // Close mpi file
    MPI_File_close(&mpiWaveFile);
}

void writeWAV(char * filename, int chunkSize, char * wavChunkBuffer, char * wavHeaderBuffer, MPI_Comm comm)
{
    // TODO: Problematic area
    // Write the header file before we write the rest of the file
    if( gRank == gRoot )
        writeHeader( filename, wavHeaderBuffer );

    // Preparing Variables to do mpi file write
    MPI_File mpiWaveFile;
    MPI_Status status;
    
    // Create file pointer to the file being read
    MPI_File_open(comm,filename,MPI_MODE_WRONLY | MPI_MODE_CREATE,MPI_INFO_NULL, &mpiWaveFile);

    // Determine reading offset
    int offset = (gRank * chunkSize) + HEADER_SIZE;

    // Set location to start reading from
    MPI_File_seek(mpiWaveFile,offset , MPI_SEEK_SET);

    // Writes the chunk of data
    MPI_File_write(mpiWaveFile, wavChunkBuffer, chunkSize, MPI_BYTE, &status);
    
    // Close mpi file
    MPI_File_close(&mpiWaveFile);
}

char * hankelize( char * vector, int length )
{
    // size of hankel matrix 
    int hankelCols = ( length + 1 ) / 2;

    // Create buffer for the hankelied matrix
    char * hankelizedMatrix = ( char * ) malloc( hankelCols*hankelCols );

    // Iterate through the hankel matrix
    int row,col,vectorOffset = 0;
    for( row = 0 ; row < hankelCols ; row++,vectorOffset++ )
    {
        for ( col = 0; col < hankelCols ; col++)
        {   
            hankelizedMatrix[row*hankelCols + col] = vector[vectorOffset + col];
        }
    }

    return hankelizedMatrix;
}

void printBuffer( char * buffer , int length )
{
    int i ;
    for ( i = 0; i < length; i++ )
    {
        printf("%c ", buffer[i]);
    }
    puts("");
}
