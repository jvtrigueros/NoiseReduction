#! /bin/bash

SHELL := /bin/bash

BINARY = noiseReduction
FLAGS = -Wall -g -c 
SCALAPACK_FLAGS = -I$$IMKLPATH -L$$MKLPATH -lmkl_scalapack -lmkl_blacs_openmpi_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lpthread -openmp

$(BINARY): main.o  MPI_Setup.o wavIO.o
	mpicc  wavIO.o main.o MPI_Setup.o  $(SCALAPACK_FLAGS) -o $(BINARY)

wavIO.o: wavIO.c
	mpicc $(FLAGS) wavIO.c 

MPI_Setup.o: MPI_Setup.c
	mpicc $(FLAGS) MPI_Setup.c 

main.o: main.c
	mpicc $(FLAGS) main.c $(SCALAPACK_FLAGS)

run:
	mpiexec $(BINARY)

clean:
	rm $(BINARY) *.o

configure:
	use intel-openmpi-1.2.7
	export IMKLPATH=/packages/intel/cmkl/10.0.2.018/include
	export MKLPATH=/packages/intel/cmkl/10.0.2.018/lib/em64t/
# mpicc -o pdgemv pdgemv.c -I$IMKLPATH -L$MKLPATH -lmkl_scalapack -lmkl_blacs_openmpi_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lpthread -openmp
